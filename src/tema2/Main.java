package tema2;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
	private static File f ;
	private static Scanner scanner;
	private static Scanner scannerin = new Scanner(System.in);	
	private static List<student> allStudents = new ArrayList<student>();
	private static int subjectChoose;
	private static int studentChoose;
	private static String name;
	
	public static void addStudent() { 
		System.out.println("Provide Student name: ");
		String name = scannerin.next();
		List<Double> list = new ArrayList<Double>();
		System.out.println("Provide Student Grades for: ");
		
		for(int j = 0;j< ServiceSubject.numberOfSubjects;j++)
		{
			System.out.println(ServiceSubject.allSubjects.get(j).getName() + ": ");
			list.add(scannerin.nextDouble());
		}
		student Student = new student(name,list);
		allStudents.add(Student);
	}
	
	public static void firstReading() 
	{
		ServiceSubject.setSubjects();
		f =new File("src\\tema2\\date.txt");
		try {
			scanner = new Scanner(f);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
		
		while ( scanner.hasNextLine() )
		{
			
			try{
				 name = scanner.next();
			List<Double> list = new ArrayList<Double>();
			for(int i=0; i< ServiceSubject.numberOfSubjects;i++) 
			{
				Double d=scanner.nextDouble();
				
				list.add(d);
			}
			
			student Student = new student(name,list);
			allStudents.add(Student);
			}catch(Exception e) {
				System.out.println("Atention your file has one more line and it is empty.");
			}
		}
		
	}
	
	private static void ShowAllStudents() {
		for(int i = 0; i < allStudents.size();i++)
		{
			System.out.println(allStudents.get(i).getName() + " has at" );
			for(int j = 0;j< ServiceSubject.numberOfSubjects;j++)
			{
				System.out.println(ServiceSubject.allSubjects.get(j).getName() + " grade :" + allStudents.get(i).getOneGrade(j));
			}
			System.out.println();
		}
	}
	
	private static void meniu() {
		System.out.println("Choose from the list below  what you what you want providing the number according to it: ");
		System.out.println("1.View all the Data");
		System.out.println("2.Add a student and his grades for various objects.");		
		System.out.println("3.Retrieve statistics calculated for the existing data.");	
		System.out.println("4.Exit");
		System.out.println("Your choise is (provide an integer number):");
		int y = 4;
		try{
			y=scannerin.nextInt();
		}catch(Exception e)
		{
			System.out.println("You didn't type a number.");
		}
		switch(y) {
		case 1:
			ShowAllStudents();
			meniu();
			break;
		case 2:
			addStudent();
			meniu();
			break;
		case 3:
			getCase3();
			meniu();
			break;
		case 4:
			System.out.println("Uploading changes .....");
			try {
				uploadChanges();
			}catch(Exception e) {
				System.out.println("Uploaded unsuccessfully!ERROR UNKNOW");
			}
			System.out.println("Thank you.GoodBye!");
			break;
		default:
			System.out.println("The number is not in the list.Please provide one in  the list." +"\n");
			meniu();
		}
		
	}
	public static void getCase3()
	{
		System.out.println("1.View all grades for a specific subject.");
		System.out.println("2.View average grade for a specific student.");		
		System.out.println("3.View average grade for a specific subject.");	
		System.out.println("4.Exit.");	
		System.out.println("Your choise is (provide an integer number):");
		int w = 4;
		try{
			w=scannerin.nextInt();
		}catch(Exception e)
		{
			System.out.println("You didn't type a number.");
		}
		switch(w) {
		case 1:
			for(int i =0;i<ServiceSubject.allSubjects.size();i++)
			{
				System.out.println(i+1 + ". " + ServiceSubject.allSubjects.get(i).getName() );
			}
			subjectChoose = scannerin.nextInt()-1;
			System.out.println(subjectChoose+1 + ". " + ServiceSubject.allSubjects.get(subjectChoose).getName() );
			ServiceSubject.allSubjects.get(subjectChoose).showAllGrades();
			meniu();
			break;
		case 2:
			for(int i =0;i<allStudents.size();i++)
			{
				System.out.println(i+1 + ". " + allStudents.get(i).getName() );
			}
			studentChoose = scannerin.nextInt()-1;
			allStudents.get(studentChoose).showAllGrades();
			System.out.println("Average grade for " + (int)(studentChoose+1) + ". " + allStudents.get(studentChoose).getName() +" is: " );
			System.out.println(allStudents.get(studentChoose).getAverageGrade());
			meniu();
			break;
		case 3:
			
			meniu();
			break;
		case 4:
			System.out.println("Thank you.GoodBye!");
			break;
		default:
			System.out.println("The number is not in the list.Please provide one in  the list." +"\n");
			meniu();
		}
	}
	
	public static void uploadChanges() throws IOException
	{

		PrintWriter writer = new PrintWriter("src\\tema2\\date.txt", "UTF-8");
		for(int i=0;i<allStudents.size();i++)
		{
			writer.println(allStudents.get(i).getName() );
			for(int j=0; j< ServiceSubject.allSubjects.size();j++)
			{
				writer.println(allStudents.get(i).getOneGrade(j));
			}
		}
	    writer.close();
	}
	
	public static void main(String[] args)
	{
		System.out.println("Loading file..." +"\n");
		firstReading();
		System.out.println("File successfully loaded" +"\n");
		meniu();
		
	}
}
