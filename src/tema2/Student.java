package tema2;

import java.util.ArrayList;
import java.util.List;

public class student {
	private String name;
	private List<Double> note = new ArrayList<Double>();
	
	public student(String name,List<Double> list) {
		this.name=name;
		this.note = list;
		for (int i = 0 ;i< note.size();i++)
		{	
			ServiceSubject.allSubjects.get(i).addOneGrade(note.get(i));
		}
	}
	
	public String getName() {
		return name;
	}
	public List<Double> getNote() {
		return note;
	}
	public void setNote(List<Double> note) {
		this.note = note;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getOneGrade(int i) {
		return this.note.get(i);
	}
	public double getAverageGrade() {
		double s=0,nr=0;
			for (int i = 0 ;i< note.size();i++)
			{	
				s += note.get(i);
				nr++;
			}
			return s/nr;
	}
	public void showAllGrades()
	{
		for (int i = 0 ;i< note.size();i++)
		{	
			System.out.println(note.get(i));
		}
	}
	
	@Override
	public String toString() {
		return  " Numele studentului : " + getName() +"\n" +  
				getOneGrade(0) + "\n" +  
				getOneGrade(1) + "\n" +  
				getOneGrade(2) + "\n" +  
				getOneGrade(3) + "\n" +  
				getOneGrade(4) + "\n" ;  

	}
	

}
