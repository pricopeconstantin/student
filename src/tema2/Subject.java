package tema2;

import java.util.ArrayList;
import java.util.List;

public class Subject {
	private String name;
	private List<Double> Grades= new ArrayList<Double>();
	

	public Subject(String name) {
		super();
		this.name = name;
	}

	public List<Double> getGrades() {
		return Grades;
	}

	public void setGrades(List<Double> grades) {
		Grades = grades;
	}

	public String getName() {
		return name;
	}

	public void addOneGrade(double grade)
	{
		Grades.add(grade);
	}
	public double getAverageGrade() {
		int s=0,nr=0;
			for (int i = 0 ;i< Grades.size();i++)
			{	
				s += Grades.get(i);
				nr++;
			}
			return s/nr;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	public void addGrade(double arg0)
	{
		Grades.add(arg0);
	}
	public void showAllGrades()
	{
		for (int i = 0 ;i< Grades.size();i++)
		{	
			System.out.println(Grades.get(i));
		} 
	}
}
