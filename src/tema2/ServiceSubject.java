package tema2;

import java.util.ArrayList;
import java.util.List;

public class ServiceSubject {
	public static int numberOfSubjects = 0;
	public static List<Subject> allSubjects = new ArrayList<Subject>(); 

	public int getNumberOfSubjects() {
		return numberOfSubjects;
	}

	static void setSubjects()
	{
		Subject s1 = new Subject("Hystory");
		numberOfSubjects++;
		Subject s2 = new Subject("English");
		numberOfSubjects++;
		Subject s3 = new Subject("Math");
		numberOfSubjects++;
		Subject s4 = new Subject("Music");
		numberOfSubjects++;
		Subject s5 = new Subject("Sport");
		numberOfSubjects++;
		allSubjects.add(s1);
		allSubjects.add(s2);
		allSubjects.add(s3);
		allSubjects.add(s4);
		allSubjects.add(s5);
		
	}
	
	
}
